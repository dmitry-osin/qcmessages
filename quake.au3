#include <MsgBoxConstants.au3>
#include <Clipboard.au3>
#include <Misc.au3>
#include <MouseOnEvent.au3>

; Bind keys
_BindKeys()

; Unbind keys
_UnbindKeys()

; Infinite loop that allows to listen keyboard events
While 1
    Sleep(100)
	$isExists = ProcessExists("QuakeChampions.exe") ; Find QC process or die

	If Not $isExists Then
		MsgBox(16, 'Error', 'QC process is not found')
		Exit
	EndIf
WEnd

Func _UnbindKeys()
	HotKeySet("", "_DoNothing")
EndFunc

Func _BindKeys()
	; Bind keyboard
	HotKeySet("{NUMPAD0}", "_QuadMessage")
	HotKeySet("{NUMPAD1}", "_ProtectionMessage")
	HotKeySet("{NUMPAD2}", "_MegaMessage")
	HotKeySet("{NUMPAD3}", "_ArmorMessage")
	HotKeySet("{NUMPAD4}", "_StickTogetherMessage")
	HotKeySet("{NUMPAD5}", "_PowerUpLowMessage")

	; Bind mouse
	_MouseSetOnEvent($MOUSE_WHEELSCROLLDOWN_EVENT, "_MouseWheel_Events")
    _MouseSetOnEvent($MOUSE_WHEELSCROLLUP_EVENT, "_MouseWheel_Events")
EndFunc

; Handle mouse events
Func _MouseWheel_Events($iEvent)
    Switch $iEvent
        Case $MOUSE_WHEELSCROLLDOWN_EVENT
            _SendMessageToChat(":D")
        Case $MOUSE_WHEELSCROLLUP_EVENT
            _SendMessageToChat("C'mon")
    EndSwitch

    Return $MOE_BLOCKDEFPROC ;Block
EndFunc

Func _ItemHasBeenTakenFunc($itemName)
	_SendMessageToChat($itemName & " has been taken")
	Sleep(18000)
	_SendMessageToChat($itemName & " in 10 seconds")
	Sleep(5000)
	_SendMessageToChat($itemName & " in 5 seconds")
	Sleep(5000)
	_SendMessageToChat($itemName & " spawned")
EndFunc

Func _DoNothing()
	; Do nothing to prevent keys event raising
EndFunc

Func _SendMessageToChat($message, $delay = 15)
	ClipPut($message)
	Send("{ENTER}")
	Sleep($delay)
	Send("^v")
	Sleep($delay)
	Send("{ENTER}")
EndFunc

; Messages
Func _QuadMessage()
	_SendMessageToChat("Go To QUAD")
EndFunc

Func _ProtectionMessage()
	_SendMessageToChat("Go To PROTECTION")
EndFunc

Func _MegaMessage()
	_SendMessageToChat("Go To MEGA")
EndFunc

Func _ArmorMessage()
	_SendMessageToChat("Go To RED ARMOR")
EndFunc

Func _StickTogetherMessage()
	_SendMessageToChat("Stick together")
EndFunc

Func _PowerUpLowMessage()
	_SendMessageToChat("POWER UP is LOW")
EndFunc
