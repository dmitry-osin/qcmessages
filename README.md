# QCMessages

## FAQ

### What is it?

This is AutoIt script allows to add vanilla Q3/QL messages to QC via binding keyboard events to AutoIt functions.

### How I can compile it?

To compile this script you should install AutoIt with editor.
I recomment to use full installation package from this [page](https://www.autoitscript.com/site/autoit/downloads/).

After the installation quake.au3 file might be opened by Scite Script Editor from Windows start menu.
When script is opened in the editor it is might be compiled via Tools -> Compile menu or just started via Tools -> Test run menu.

### How to align shortcuts to my needs?

All shortcuts available on the top of the script body. 

`HotKeySet("{NUMPAD0}", "_QuadMessage")
HotKeySet("{NUMPAD1}", "_ProtectionMessage")
HotKeySet("{NUMPAD2}", "_MegaMessage")
HotKeySet("{NUMPAD3}", "_ArmorMessage")
HotKeySet("{NUMPAD4}", "_StickTogetherMessage")
HotKeySet("{NUMPAD5}", "_PowerUpLowMessage")`

Here in curly braces listed shortcuts, you can modify it to your requirements.
PS. The second parameter of HotKeySet function is function name that will be invoked by shortcut, please be sure that second parameter function name equals to function name which is declared below.
